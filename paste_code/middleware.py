from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy as reverse
from django.utils import translation
from django.conf import settings
from django.core.urlresolvers import resolve


class RedirectMiddleware(object):
    def is_have_access(self, view_name):
        try:
            return view_name in settings.ALLOWED_EXTERNAL_ACCESS_VIEW
        except:
            pass

    def process_request(self, request):
        current_language = translation.get_language()
        current_url = str(request.get_full_path())
        view = resolve(current_url.split('?')[0])
        if self.is_have_access(current_url):
            return
        if not request.user.is_authenticated():
            login_url = str(reverse('login'))
            if view.view_name != 'login':
                url = current_url
                if 'logout' in view.view_name:
                    url = '/'
                return redirect(login_url + '?next={}'.format(url))
        else:
            user = request.user
            try:
                if user.profile.language != current_language:
                    translation.activate(user.profile.language)
                    request.session[translation.LANGUAGE_SESSION_KEY] = user.profile.language
            except Exception as e:
                print e.message
            if not request.user.profile.changed_password and view.view_name != 'profile:change_password':
                return redirect(reverse('profile:change_password'))
