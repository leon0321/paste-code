"""claro_invoice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import logout, login

from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm

from userprofile.forms import AuthenticationForm
from paste.views import PasteCodeCreateView

urlpatterns = [
    url(r'^$', PasteCodeCreateView.as_view(), name='add'),
    url(r'^(?P<pk>[-\w]+)/$', PasteCodeCreateView.as_view(), name='edit'),
    url(r'^paste/', include('paste.urls', app_name='paste', namespace='paste')),
    url(r'^profile/', include('userprofile.urls', app_name='profile', namespace='profile')),
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += [
    url(r'^login/$', login, {'template_name': 'pages/login.html',
                             'authentication_form': AuthenticationForm}, name='login'),
    url(r'^logout/$', logout, {'next_page': '/login'}, name='logout'),
]

urlpatterns += [
    url('^register/', CreateView.as_view(
        template_name='register.html',
        form_class=UserCreationForm,
        success_url='/'
    ), name='register'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
