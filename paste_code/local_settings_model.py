DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'nokia_paste',  # Or path to database file if using sqlite3.
        'USER': 'leon',
        'PASSWORD': 'leon',
        'HOST': 'localhost',
        'PORT': '',  # Set to empty string for default.
    }
}

AUTH_PASSWORD_VALIDATORS = [] # Disable auth validators