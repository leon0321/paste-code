from django import template

register = template.Library()


@register.filter
def addclass(field, css):
    field.field.widget.attrs['class'] = field.field.widget.attrs.get('class', '') + ' ' + css
    return field


@register.filter
def get_css_class(field):
    return field.field.widget.attrs.get['class']


@register.filter
def setclass(field, css):
    field.field.widget.attrs['class'] = css
    return field


@register.filter
def set_rows(field, rows):
    field.field.widget.attrs['rows'] = rows
    return field


@register.filter
def placeholder(field, value):
    field.field.widget.attrs['placeholder'] = value
    return field


@register.filter(name='qs')
def query_string(field, value):
    return '&' + str(field) + "=" + str(value)


@register.filter
def get_range(value):
    """
      Filter - returns a list containing range made from given value
      Usage (in template):

      <ul>{% for i in 3|get_range %}
        <li>{{ i }}. Do something</li>
      {% endfor %}</ul>

      Results with the HTML:
      <ul>
        <li>0. Do something</li>
        <li>1. Do something</li>
        <li>2. Do something</li>
      </ul>

      Instead of 3 one may use the variable set in the views
    """
    return range(1, int(value) + 1)
