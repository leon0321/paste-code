from django.http.response import HttpResponse
from django.contrib.auth.models import User
from django.views.generic import CreateView, DetailView, ListView
from django.core.urlresolvers import reverse

from paste.choices import HIGHLIGHT_LANGUAGES
from paste.models import PasteCode, LanguageCode


# Create your views here.


class PasteCodeCreateView(CreateView):
    model = PasteCode
    fields = ('language', 'comment', 'code')

    def get_success_url(self):
        return reverse('paste:detail', args=(self.object.id,))

    def get_initial(self):
        initial = super(PasteCodeCreateView, self).get_initial()
        if not self.kwargs.get('pk'):
            initial['language'] = LanguageCode.objects.filter(language_slug=HIGHLIGHT_LANGUAGES.Python).last()
        return initial

    def get_form_kwargs(self):
        kwargs = super(PasteCodeCreateView, self).get_form_kwargs()
        if self.kwargs.get('pk') and PasteCode.objects.filter(id=self.kwargs.get('pk')).exists():
            kwargs['instance'] = PasteCode.objects.get(id=self.kwargs.get('pk'))
        return kwargs

    def form_valid(self, form):
        paste_code = form.save(commit=False)
        paste_code.id = None
        if self.request.user.is_authenticated():
            user = self.request.user
        else:
            user = User.objects.first()
        paste_code.user = user
        paste_code.save()
        return super(PasteCodeCreateView, self).form_valid(form)


class PasteCodeDetailView(DetailView):
    model = PasteCode


class PasteCodeDetailCodeView(DetailView):
    model = PasteCode

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return HttpResponse(self.object.code)


class PasteCodeListView(ListView):
    model = PasteCode
