from django.conf.urls import patterns, url

from paste import views

urlpatterns = [
    url(r'^list/$', views.PasteCodeListView.as_view(), name='list'),
    url(r'^(?P<pk>[-\w]+)/$', views.PasteCodeDetailView.as_view(), name='detail'),
    url(r'^detail__code/(?P<pk>[-\w]+)/$', views.PasteCodeDetailCodeView.as_view(), name='detail__code'),
]
