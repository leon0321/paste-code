from django.contrib import admin
from paste.models import PasteCode, LanguageCode
from django.utils.translation import ugettext_lazy as _


# Register your models here.
class PasteCodeAdmin(admin.ModelAdmin):
    list_display = ('user', 'language', 'comment',)
    exclude = ('code',)
    readonly_fields = ('user', 'language', 'comment', 'pre_code')

    def pre_code(self, obj):
        return u'<pre><br>{}</pre>'.format(obj.code)

    pre_code.allow_tags = True
    pre_code.short_description = _('code')


class LanguageCodeAdmin(admin.ModelAdmin):
    list_display = ('language',)

admin.site.register(PasteCode, PasteCodeAdmin)
admin.site.register(LanguageCode, LanguageCodeAdmin)
