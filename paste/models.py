from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from paste.choices import HIGHLIGHT_LANGUAGES


class LanguageCode(models.Model):
    language = models.CharField(_('language'), unique=True, max_length=144)
    language_slug = models.CharField(_('Highlight language'), max_length=144, default='', choices=HIGHLIGHT_LANGUAGES)

    def __unicode__(self):
        return u'{}'.format(self.language)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.language_slug = slugify(self.language)
        super(LanguageCode, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                       update_fields=update_fields)

    class Meta:
        ordering = ('language',)


class PasteCode(models.Model):
    code = models.TextField(_('code'))
    language = models.ForeignKey('paste.LanguageCode', verbose_name=_('language'))
    user = models.ForeignKey('auth.User', verbose_name=_('User'))
    comment = models.CharField(_('comment'), max_length=255, null=True, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.language, self.user, self.comment)
