from __future__ import unicode_literals
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UserProfileConfig(AppConfig):
    name = 'userprofile'
    label = 'userprofile'
    verbose_name = _("Users profiles")

    def ready(self):
        """
        Import all signals
        """
        import signals