from __future__ import absolute_import
from __future__ import unicode_literals
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from userprofile.choices import PREFIX, LANGUAGE
from userprofile.models import UserProfile

from django.contrib.auth.forms import AuthenticationForm as OriginalAuthenticationForm, PasswordChangeForm


class UserForm(forms.ModelForm):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    email = forms.CharField(required=False)
    picture = forms.ImageField(required=False)
    prefix = forms.ChoiceField(choices=PREFIX, required=False)
    language = forms.ChoiceField(choices=LANGUAGE, initial=LANGUAGE.EN)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        if kwargs.get('instance'):
            kwargs.update(initial={
                'prefix': kwargs.get('instance').profile.prefix,
                'language': kwargs.get('instance').profile.language,
            })

        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({
            'placeholder': _('Type your first name here'),
            'class': 'form-control',
            'disabled': 'disabled',
        })
        self.fields['last_name'].widget.attrs.update({
            'placeholder': _('Type your last name here'),
            'class': 'form-control',
            'disabled': 'disabled'
        })
        self.fields['email'].widget.attrs.update({
            'placeholder': _('Type your email address here'),
            'class': 'form-control',
            'disabled': 'disabled'
        })
        self.fields['prefix'].widget.attrs.update({
            'class': 'form-control edit-form',
            'disabled': 'disabled'
        })
        self.fields['language'].widget.attrs.update({
            'class': 'form-control edit-form',
            'disabled': 'disabled'
        })
        self.fields['picture'].widget.attrs.update({
            'class': 'hidden'
        })

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        # user = User.objects.get(pk=self.instance.id)
        profile = user.profile
        if self.cleaned_data['picture']:
            profile.picture = self.cleaned_data['picture']

        if self.cleaned_data.get('prefix'):
            profile.prefix = self.cleaned_data.get('prefix')

        if self.cleaned_data['language']:
            profile.language = self.cleaned_data.get('language')

        profile.save()


class ProfileForm(forms.ModelForm):
    # follow_up = forms.MultipleChoiceField(choices=FOLLOW_UP, widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = UserProfile
        exclude = ['user', ]

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['notification_day'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['notification_hour'].widget.attrs.update({
            'class': 'form-control custom-time',
            'placeholder': 'Click to display time picker'
        })


class AuthenticationForm(OriginalAuthenticationForm):
    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password. "
                           "Note that both fields may be case-sensitive."),
        'inactive': _('Access Denied, please contact the <strong><a href="mailto:support.zina@nokia.com" target="_top">'
                      'support</a></strong> team for more information.'),
    }


class ChangePasswordForm(PasswordChangeForm):
    def save(self, commit=True):
        user = super(ChangePasswordForm, self).save(commit=commit)
        user.profile.changed_password = True
        user.profile.save()
        return user
