from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.edit import BaseFormView
from userprofile.forms import UserForm, ProfileForm


class ProfileView(TemplateView):
    template_name = 'profile/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)

        context.update({
            'user_form': UserForm(instance=self.request.user),
            'profile_form': ProfileForm(instance=self.request.user.profile),
        })
        return context


profile = ProfileView.as_view()


class UpdateProfileView(BaseFormView):
    form_class = UserForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateProfileView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwarg = super(UpdateProfileView, self).get_form_kwargs()
        kwarg.update({
            'instance': self.request.user
        })
        return kwarg

    def form_invalid(self, form):
        return JsonResponse(data={'done': False, 'errors': form.errors})

    def form_valid(self, form):
        form.save()
        return JsonResponse(data={
            'done': True,
            'redirect': reverse('profile:profile')
        }, safe=False)


update_profile = UpdateProfileView.as_view()


class NotificationProfileView(BaseFormView):
    form_class = ProfileForm

    def dispatch(self, request, *args, **kwargs):
        return super(NotificationProfileView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwarg = super(NotificationProfileView, self).get_form_kwargs()
        kwarg.update({
            'instance': self.request.user.profile
        })
        return kwarg

    def form_valid(self, form):
        form.save()
        return JsonResponse(data={
            'done': True,
            'redirect': reverse('profile:profile')
        }, safe=False)

    def form_invalid(self, form):
        return JsonResponse(data={'done': False, 'errors': form.errors})

notification_profile_view = NotificationProfileView.as_view()