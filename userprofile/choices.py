from django.utils.translation import ugettext as _
from django_choices_flow import Choices


class GENDER(Choices):
    FEMALE = 'female', _('Female')
    MALE = 'male', _('Male')


class PREFIX(Choices):
    MS = 'ms', _('Ms')
    MISS = 'miss', _('Miss')
    MRS = 'mrs', _('Mrs')
    MR = 'mr', _('Mr')
    DR = 'dr', _('Dr')
    PRES = 'pres', _('Pres')
    OFC = 'ofc', _('Ofc')


class LANGUAGE(Choices):
    EN = 'en', _('English')
    ES = 'es', _('Spanish')
