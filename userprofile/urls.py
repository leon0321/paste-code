from django.conf.urls import url
from userprofile import views
from django.contrib.auth import views as auth_views

# USER = '(?P<username>([0-9]|[a-z]|[A-Z])+)'
from userprofile.forms import ChangePasswordForm

urlpatterns = [
    url(r'^$', views.profile, name='profile'),
    url(r'^update/$', views.update_profile, name='update'),
    url(r'^notifications/$', views.notification_profile_view, name='notification_update'),
    url('^change-password/', auth_views.password_change,
        {'template_name': 'profile/form_change_password.html', 'post_change_redirect': '/',
         'password_change_form': ChangePasswordForm}, name='change_password'),
]
