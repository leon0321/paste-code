from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django_thumbs.db.models import ImageWithThumbsField
from django.utils.translation import ugettext_lazy as _
from userprofile.choices import GENDER, PREFIX, LANGUAGE


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile', verbose_name=_('User'))
    picture = ImageWithThumbsField(upload_to='profiles/pictures/', null=True, blank=True, verbose_name=_('Picture'))
    gender = models.CharField(_('gender'), choices=GENDER, max_length=10, default=GENDER.MALE)
    prefix = models.CharField(_('prefix'), choices=PREFIX, max_length=10, default=PREFIX.MS)
    language = models.CharField(_('language'), choices=LANGUAGE, max_length=3, default=LANGUAGE.EN)
    changed_password = models.BooleanField(_('changed password'), default=False)

    def __unicode__(self):
        return self.user.username

    def get_prefix(self):
        return _(u'{}. {}').format(self.get_prefix_display(), self.user.get_full_name())

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                p = UserProfile.objects.get(user=self.user)
                self.pk = p.pk
            except UserProfile.DoesNotExist:
                pass

        super(UserProfile, self).save(*args, **kwargs)
