from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from models import UserProfile


@receiver(post_save, sender=User)
def create_profile(sender, **kwargs):
    user = kwargs.pop('instance')
    profile = UserProfile.objects.filter(user=user)
    if not profile.exists():
        p = UserProfile()
        p.user = user
        p.save()
